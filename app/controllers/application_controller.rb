class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
    def set_flickr
        FlickRaw.api_key = ENV["API_KEY"]
        FlickRaw.shared_secret= ENV["SHARED_SECRET"]
        flickr.access_token = ENV["ACCESS_TOKEN"]
        flickr.access_secret = ENV["ACCESS_SECRET"]
    end
end
