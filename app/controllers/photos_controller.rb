class PhotosController < ApplicationController
    require "flickraw"
    before_action :set_flickr, only: [:create, :destroy, :index]
    def index
        @photos = Photo.all
    end
    def show
       @photo = Photo.find(params[:id]) 
    end
    def new
        @photo = Photo.new
    end
    def create
        photo_id = flickr.upload_photo params[:photo].tempfile.path, :title => params[:title], :description => params[:desc]
        photo_url = FlickRaw.url_o(flickr.photos.getInfo(photo_id: photo_id))
        @photo = Photo.new
        @photo.photo_id = photo_id
        @photo.photo_url = photo_url
        @photo.title = params[:title]
        @photo.desc = params[:desc]
        respond_to do |format|
            if @photo.save
                format.html { redirect_to @photo }
                format.json { render :show, status: :created, location: @photo }
            else
                format.html { render action: ‘new’ }
                format.json { render json: @photo.errors, status: :unprocessable_entity }
            end
    end
    def destroy
        flickr.photos.delete(photo_id: @photo.photo_id)
        @photo.destroy
        respond_to do |format|
        format.html { redirect_to photos_url }
        format.json { head :no_content }
        end
    end




    end
    private
    def photo_params
       params.require(:photo).permit(:title, :desc) 
    end
end
