Rails.application.routes.draw do
  root "photos#index"
  resources :photos
  resources :static_pages, only: [:index]
end
