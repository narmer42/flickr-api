class CreatePhotos < ActiveRecord::Migration[5.0]
  def up
    create_table :photos do |t|
      t.string :title
      t.string :desc
      t.string :photo_url
      t.string :photo_id
      t.timestamps
    end
  end
  def down
    drop_table :photos
  end
end
